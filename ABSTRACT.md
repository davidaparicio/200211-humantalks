# Security By Design

## 20-02-11 : [Human Talks février](https://humantalks.com/talks/1526-security-by-design)

Un ensemble d'astuces pour prendre en compte la sécurité dès la conception. Je partagerai mon expérience au sein d'une implémentation à AMADEUS (Sophia-Antipolis).
