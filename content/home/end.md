+++
weight = 3
+++
## Why?

![OWASP](img/owasp.png)
[OWASP Top 10](https://tinyurl.com/api-owasp)

---
## Gendarmerie Nationale

> L’entrée en vigueur du [RGPD](https://fr.wikipedia.org/wiki/R%C3%A8glement_g%C3%A9n%C3%A9ral_sur_la_protection_des_donn%C3%A9es) modifie la posture des acteurs (des traitements) qui doivent tenir compte des impératifs de sécurité dès la conception d’un produit ainsi que son cycle de vie. Le label [« by design »](https://www.gendarmerie.interieur.gouv.fr/crgn/content/download/1033/16080/) devient un label de qualité qui constituera un atout commercial.

---
## 2022

* 90% des développements logiciels se déclaront DevSecOps (+40% 2019)
* 25% des développements IT selon DevOps (+10% 2019)

[Gartner/Techwire](https://www.techwire.net/sponsored/integrating-security-into-the-devsecops-toolchain.html)

___
## Et pour éviter cela
![Windows 3.1](img/why_sec.png)

---
## Pour aller plus loin

* [Sophia Security Camp 2019](http://www.telecom-valley.fr/sophia-security-camp-2019/)
* [ANSSI Sécurité Agile](https://www.ssi.gouv.fr/administration/guide/agilite-et-securite-numeriques-methode-et-outils-a-lusage-des-equipes-projet/) (Atelier d'analyse de risque)
![Guide de l'ANSSI, Agilité & Sécurité numériques](img/anssi_agile.jpg)

---
## David Aparicio
{{% note %}}Petite intro rapide{{% /note %}}
Double diplôme INSA de Lyon / UNICAMP (Brésil)

15/ Facebook Open Academy / MIT AppInventor

17/ Dev(Sec)Ops @ AMADEUS (Nice, 2 ans)

19/ DataOps @ OVHCloud (Lyon)

---
#### Questions ?
{{% note %}}Merci et si vous voulez en savoir plus, je donnerai un meetup
ce jeudi sur la mise en place de cette technique à AMADEUS{{% /note %}}

![chien](img/security_container_small.gif)
###### ReX au [Meetup GDG](https://www.meetup.com/GDG-Cloud-Lyon/events/lpnvdpybcdbrb/) ce Jeudi 19h / @dadideo

<!-- 
---
##### Snyk Key Takeaways

![Snyk Key Takeaways](/img/snyk_takeaways.png)
[The state of open source security – 2019](https://res.cloudinary.com/snyk/image/upload/v1551172581/The-State-Of-Open-Source-Security-Report-2019-Snyk.pdf)
-->
