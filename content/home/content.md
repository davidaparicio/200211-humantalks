+++
weight = 2
+++
### Sécurité dès la conception

95/ SdD-"Privacy By Design"

97/ Loi allemande

10-12/ Congrès

---
### DevSecOps

![DevSecOps](img/devsecops.png)
[dodcio.defense.gov](https://dodcio.defense.gov/Portals/0/Documents/DoD%20Enterprise%20DevSecOps%20Reference%20Design%20v1.0_Public%20Release.pdf?ver=2019-09-26-115824-583)

---
## Techniques

Pas copier-coller **StackOverFlow**

98% snippets sur la sécurité/crypto
sont *insecures*

Fisher et al., 2017; Nadi et al., 2016; Das et al, 2014
[Prevent cryptographic pitfalls by design](https://archive.fosdem.org/2019/schedule/event/crypto_pitfalls/)

---
## Attention avec Docker
![Docker vulnerabilities](img/snyk_rapport.png)
[The state of open source security – 2019](https://snyk.io/opensourcesecurity-2019/)

---
## Une part de bonnes pratiques

{{% fragment %}}* Principe de moindre privilège !root {{% /fragment %}}
{{% fragment %}}* Diminuer la surface d'attaque (scratch, distroless){{% /fragment %}}
{{% fragment %}}* Pas de données sensibles dans les images {{% /fragment %}}
{{% fragment %}}* Mettre à jour infra/docker images (CI/CD|[GitOps](https://www.infoq.com/news/2020/02/wksctl-kubernetes-gitops/)) {{% /fragment %}}
{{% fragment %}}* Ne pas afficher de stacktrace (pas debug) {{% /fragment %}}
{{% fragment %}}* Ni de version/nom de framework {{% /fragment %}}
{{% fragment %}}* Vérifier les entrées/sorties (injection/XSS) {{% /fragment %}}
{{% fragment %}}* PaaS (BUILD/RUN) 🇪🇺 OVHCloud/CleverCloud {{% /fragment %}}

---
## Outils

{{% fragment %}}* [Linter](https://en.wikipedia.org/wiki/Lint_(software)) {{% /fragment %}}
{{% fragment %}}* [SonarQube](https://www.sonarqube.org/) {{% /fragment %}}
{{% fragment %}}* [Argo](https://argoproj.github.io/) {{% /fragment %}}
{{% fragment %}}* [Notary](https://docs.docker.com/notary/getting_started/) (Sign. Docker image) {{% /fragment %}}
{{% fragment %}}* [Clair](https://coreos.com/clair/docs/latest/)/[Anchore](https://anchore.com/)/[Dagda](https://github.com/eliasgranderubio/dagda) (CVE Analysis) {{% /fragment %}}
{{% fragment %}}* [OpenSCAP](https://www.open-scap.org/) {{% /fragment %}}
{{% fragment %}}* [Cilium](https://cilium.io/) (Network) {{% /fragment %}}
{{% fragment %}}* [gVisor](https://github.com/google/gvisor) (Sandbox) {{% /fragment %}}
{{% fragment %}}* [Istio](https://istio.io)/[maesh](https://containo.us/maesh/) (SSL/Service Mesh) {{% /fragment %}}
{{% fragment %}}* [Falco](https://fosdem.org/2020/schedule/event/kubernetes/) (Monitoring/Agent) {{% /fragment %}}
{{% fragment %}}* [42Crunch](https://42crunch.com/api-security/) (API Scanner) {{% /fragment %}}

<!-- 
## [Falco](https://sysdig.com/opensource/falco/)
![Falco](img/falco.png)
-->
