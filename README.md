# Security By Design

[![Run in GitLab](https://svgshare.com/i/aKZ.svg)](https://davidaparicio.gitlab.io/200211-humantalks/)

Slides "Security By Design"
https://humantalks.com/cities/lyon/events/533
https://www.meetup.com/HumanTalks-Lyon/events/268115464/

🚀 How to run it ?

```bash
hugo server
open localhost:1313
# To print into PDF
# -> localhost:1313/?print-pdf&showNotes=false&pdfSeparateFragments=false
```

🕸️ How install dependencies ?

```bash
brew install hugo
git submodule update --recursive --remote --init
# or
# git submodule init && \
# git submodule add git@github.com:dzello/reveal-hugo.git themes/reveal-hugo
```